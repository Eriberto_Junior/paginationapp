package com.eriberto.paginationapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import androidx.paging.PagingData
import com.eriberto.paginationapp.network.CharacterData
import java.util.concurrent.Executors

class MainActivityViewModel : ViewModel() {

    var charactersList: LiveData<PagedList<CharacterData>>? = null

    init {
        initPaging()
    }

    private fun initPaging() {
        val factory = CharacterListDataSourceFactory()
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(30)
            .build()
        val executor = Executors.newFixedThreadPool(5)
        charactersList = LivePagedListBuilder<Int, CharacterData>(factory, config)
            .setFetchExecutor(executor)
            .build()
    }

    fun getRecyclerListObserver(): LiveData<PagedList<CharacterData>>? {
        return charactersList
    }

}