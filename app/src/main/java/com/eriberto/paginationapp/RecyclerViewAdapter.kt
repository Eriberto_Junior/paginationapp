package com.eriberto.paginationapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eriberto.paginationapp.network.CharacterData

class RecyclerViewAdapter :
    PagedListAdapter<CharacterData, RecyclerViewAdapter.MyViewHolder>(DiffUtilCallback()) {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvName = view.findViewById<TextView>(R.id.tvName)
        val tvSpecies = view.findViewById<TextView>(R.id.tvSpecies)
        val imageThumb = view.findViewById<ImageView>(R.id.imageThumb)

        fun bind(data: CharacterData) {
            tvName.text = data.name
            tvSpecies.text = data.species

            val url = data.image
            Glide.with(imageThumb)
                .load(url)
                .circleCrop()
                .into(imageThumb)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflate =
            LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_row, parent, false)

        return MyViewHolder(inflate)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }
}