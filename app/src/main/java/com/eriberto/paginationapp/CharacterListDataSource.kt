package com.eriberto.paginationapp

import androidx.paging.PageKeyedDataSource
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.eriberto.paginationapp.network.CharacterData
import com.eriberto.paginationapp.network.RetroInstance
import com.eriberto.paginationapp.network.RetroService
import com.eriberto.paginationapp.network.RickAndMortyList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CharacterListDataSource : PageKeyedDataSource<Int, CharacterData>() {

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, CharacterData>) {

        val retroInstance = RetroInstance.getRetroInstance().create(RetroService::class.java)
        val call = retroInstance.getDataFromApi(params.key)
        call.enqueue(object : Callback<RickAndMortyList> {
            override fun onResponse(
                call: Call<RickAndMortyList>,
                response: Response<RickAndMortyList>
            ) {
                if (response.isSuccessful)
                    response.body()?.results?.let { callback.onResult(it, params.key + 1) }
            }

            override fun onFailure(call: Call<RickAndMortyList>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, CharacterData>) {
        TODO("Not yet implemented")
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, CharacterData>
    ) {
        val retroInstance = RetroInstance.getRetroInstance().create(RetroService::class.java)
        val call = retroInstance.getDataFromApi(1)
        call.enqueue(object : Callback<RickAndMortyList> {
            override fun onResponse(
                call: Call<RickAndMortyList>,
                response: Response<RickAndMortyList>
            ) {
                if (response.isSuccessful)
                    response.body()?.results?.let { callback.onResult(it, null, 2) }
            }

            override fun onFailure(call: Call<RickAndMortyList>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }
//Response{protocol=h2, code=200, message=, url=https://rickandmortyapi.com/api/character/?page=1}
}
