package com.eriberto.paginationapp

import androidx.recyclerview.widget.DiffUtil
import com.eriberto.paginationapp.network.CharacterData

class DiffUtilCallback : DiffUtil.ItemCallback<CharacterData>() {
    override fun areItemsTheSame(oldItem: CharacterData, newItem: CharacterData): Boolean {
        return oldItem.name == newItem.name
    }

    override fun areContentsTheSame(oldItem: CharacterData, newItem: CharacterData): Boolean {
        return oldItem.name == newItem.name && oldItem.species == newItem.species
    }
}