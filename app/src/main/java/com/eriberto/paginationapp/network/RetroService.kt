package com.eriberto.paginationapp.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface RetroService {
    @GET("character/")//character/?page=1
    fun getDataFromApi(@Query("page") page:Int): Call<RickAndMortyList>
}